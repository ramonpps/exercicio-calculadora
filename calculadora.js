var input = document.querySelector(".nota")
var botao = document.querySelector('.botao')
var log = document.querySelector('.notas')
var botao_2 = document.querySelector('.calculadora')
var lista = []
var soma = 0


botao.addEventListener("click", () => {
    if (input.value <= 10 && input.value >= 0) {
        log.innerHTML += input.value + "<br>"
        lista.push(Number(input.value))
    } else {
        window.alert('A nota digitada é inválida, por favor, insira uma nota válida')
    }
})


botao_2.addEventListener("click", () => {
    for (let i of lista) {
        soma += i
    }
    media = soma/lista.length
    document.querySelector('.notadisplay').innerHTML +='<h3>'+media.toFixed(2)+'</h3>'
})